import re

class hand:

    _seat_re = re.compile('^Seat \d+: (?P<name>.+?) \(\$.*')

    _sb_re = re.compile('(?P<name>.+?): posts small blind \$(?P<amount>\d+(\.\d+)?)')
    _bb_re = re.compile('(?P<name>.+?): posts big blind \$(?P<amount>\d+(\.\d+)?)')
    _sb_bb_re = re.compile('(?P<name>.+?): posts small & big blinds \$(?P<amount>\d+(\.\d+)?)')
    _ante_re = re.compile('(?P<name>.+?): posts the ante \$(?P<amount>\d+(\.\d+)?)')

    _bet_call_re = re.compile('(?P<name>.+?): (bets|calls) \$(?P<amount>\d+(\.\d+)?)')
    _raise_re = re.compile('(?P<name>.+?): raises .* to \$(?P<amount>\d+(\.\d+)?)')

    _uncalled_re = re.compile('Uncalled bet \(\$(?P<amount>\d+(\.\d+)?)\) returned to (?P<name>.+?)$')
    _collected_re = re.compile('(?P<name>.+?) collected \$(?P<amount>\d+(\.\d+)?) from pot')
    _cashed_out_re = re.compile('(?P<name>.+?) cashed out the hand for \$(?P<amount>\d+(\.\d+)?)')

    _rake_re = re.compile('.* \| Rake \$(?P<amount>\d+(\.\d+)?)')

    def __init__(self, hand_data):
        self.dusd = {}
        self.sdusd = {}
        self.dbb = {}
        self.register_players(hand_data)

        self.bigblind = self.get_big_blind(hand_data)

        self.cashedout = float(0.0)
#        for l in hand_data:
#            print(l, end = '')

        if self.process(hand_data) == -1:
            return

        for k in self.dusd.keys():
            self.dbb[k] = self.dusd[k] / self.bigblind

#        for k in self.dusd.keys():
#            print('\t\t{}: {}'.format(k, self.dusd[k]))

        self.validate(hand_data)

#       import os
#       os.system('clear')

    def validate(self, hand_data):
        all_wins = sum([a for a in self.dusd.values() if a > 0])
        all_losses = sum([a for a in self.dusd.values() if a < 0])

        #print(hand_data[0], end = '')
        allowed_error = float(0.001)
        error = (all_wins + self.rake) - abs(all_losses)

        if error > allowed_error:
            print('Validation failed! - error:{}'.format(error))

            print(all_wins)
            print(all_losses)
            print(self.rake)
            print(self.cashedout)
            for k in self.dusd.keys():
                print('\t\t{}: {}'.format(k, self.dusd[k]))

    def register_players(self, hand_data):
        for l in hand_data:
            if '*** HOLE CARDS ***' in l:
                return
            found = self._seat_re.match(l)
            if found is not None:
                self.dusd[found.group('name')] = float(0.0)
                self.sdusd[found.group('name')] = float(0.0)
                self.dbb[found.group('name')] = float(0.0)

    def advance_street(self):
#        print ('***   ***   ***')
        for k in self.sdusd.keys():
            self.dusd[k] += self.sdusd[k]
            self.sdusd[k] = float(0.0)


    def process(self, hand_data):
        for l in hand_data:

            found = self._sb_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] -= float(found.group('amount'))
#                print(l, end = '')
#                print(found.group('name'))
#                print(self.sdusd[found.group('name')], end = '')
#                print('')
                continue

            found = self._bb_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] -= float(found.group('amount'))
#                print(l, end = '')
#                print(found.group('name'))
#                print(self.sdusd[found.group('name')], end = '')
#                print('')
                continue

            found = self._sb_bb_re.match(l)
            if found is not None:
                self.dusd[found.group('name')] -= (float(found.group('amount')) - self.bigblind)
                self.sdusd[found.group('name')] -= self.bigblind
#                print(l, end = '')
#                print(found.group('name'))
#                print(self.sdusd[found.group('name')], end = '')
#                print('')
                continue

            found = self._ante_re.match(l)
            if found is not None:
                # Magic@
                self.dusd[found.group('name')] -= float(found.group('amount'))
 #               print(l, end = '')
 #               print(found.group('name'))
 #               print(self.sdusd[found.group('name')], end = '')
 #               print('')
                continue

            found = self._bet_call_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] -= float(found.group('amount'))
 #               print(l, end = '')
 #               print(found.group('name'))
 #               print(self.sdusd[found.group('name')], end = '')
 #               print('')
                continue

            found = self._raise_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] = -float(found.group('amount'))
  #              print(l, end = '')
  #              print(found.group('name'))
  #              print(self.sdusd[found.group('name')], end = '')
  #              print('')
                continue

            found = self._uncalled_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] += float(found.group('amount'))
   #             print(l, end = '')
   #             print(found.group('name'))
   #             print(self.sdusd[found.group('name')], end = '')
   #             print('')
                continue

            found = self._collected_re.match(l)
            if found is not None:
                self.sdusd[found.group('name')] += float(found.group('amount'))
 #               print(l, end = '')
 #               print(found.group('name'))
 #               print(self.sdusd[found.group('name')], end = '')
 #               print('')
                continue

            found = self._rake_re.match(l)
            if found is not None:
                self.rake = float(found.group('amount'))
 #               print(l, end = '')
 #               print('rake = ' + str(self.rake))
                continue

            if '*** FLOP ***' in l:
                self.advance_street()
                continue
            if '*** TURN ***' in l:
                self.advance_street()
                continue
            if '*** RIVER ***' in l:
                self.advance_street()
                continue
            # Beautify this!
            if 'Board [' in l:
                self.advance_street()
                return 0

            if 'Hand cancelled' in l:
                return -1

    def get_big_blind(self, hand_data):
        for l in hand_data:
            if "posts big blind" in l:
                return float(l.split(' ')[-1].strip('$'))
