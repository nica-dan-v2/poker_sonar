import fileutils
import processing

hh_files = fileutils.get_hh_files('./history_files')

print(hh_files)

delta_bb = {}
n_hands = {}

for hh_file in hh_files:
    print('Processing {0} ...'.format(hh_file))
    
    all_hh = fileutils.get_hh_from_file(hh_file)
    for hh in all_hh:

#        print(hh)

        #pretty_hh = ''
        #for l in hh:
        #    pretty_hh += l
        #print(pretty_hh)

        if len(hh) > 10:
            hand = processing.hand(hh)

            for k in hand.dbb.keys():
                if k not in delta_bb.keys():
                    delta_bb[k] = float(0.0)
                    n_hands[k] = int(0)
                delta_bb[k] += hand.dbb[k]
                n_hands[k] += 1


            #exit(0)

print('*' * 50)
for k in delta_bb.keys():
    print('{} : {} bb : {} hands'.format(k, (delta_bb[k] / n_hands[k]) * 100, n_hands[k]))

