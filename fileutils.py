import os

def get_hh_files(dirpath):
    assert(os.path.isdir(dirpath))

    files = []
    for f in os.listdir(dirpath):
        fp = os.path.join(dirpath, f)
        if os.path.isfile(fp):
            files.append(fp)
    return files

def get_hh_from_file(filepath):
    assert(os.path.isfile(filepath))

    all_hh = []
    last_hh = []
    f = open(filepath, 'r')
    try:
        for l in f:
            if l.find('PokerStars') != -1:
                all_hh.append(last_hh)
                last_hh = [l]
                continue
            last_hh.append(l)
        all_hh.append(last_hh)
    except:
        print('error')
        exit(0)
        return all_hh


    return all_hh
